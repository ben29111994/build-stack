﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleInputNamespace;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class GameController : MonoBehaviour
{
    public static GameController instance;
    float currentAngle, lastAngle, myAngle;
    public GameObject winPanel;
    public GameObject losePanel;
    Vector3 startPos, endPos, dir;
    public GameObject ground;
    public float hue;
    Color currentColor;
    public GameObject lane1Stack;
    public GameObject lane2Stack;
    bool isCheck = true;
    public List<Mesh> listMesh = new List<Mesh>();
    public Transform currentCircle;
    bool mouseLock = false;

    // Start is called before the first frame update
    void Start()
    {
        Application.targetFrameRate = 60;
        Physics.gravity = new Vector3(0, -100, 0);
        instance = this;
        currentCircle = transform;
    }

    // Update is called once per frame
    void Update()
    {
        //hue += 1 / 10000f;
        //currentColor = Color.HSVToRGB(hue, 0.7f, 0.7f);
        //ground.GetComponent<MeshRenderer>().material.color = currentColor;
        //ground.GetComponent<MeshRenderer>().material.SetColor("_EmissionColor", currentColor);
        //if (Input.GetMouseButton(0))
        //{
        //    float angle;
        //    var mouse_pos = Input.mousePosition;
        //    mouse_pos.z = -20;
        //    var object_pos = Camera.main.WorldToScreenPoint(transform.position);
        //    mouse_pos.x = mouse_pos.x - object_pos.x;
        //    mouse_pos.y = mouse_pos.y - object_pos.y;
        //    angle = Mathf.Atan2(mouse_pos.y, mouse_pos.x) * Mathf.Rad2Deg;
        //    var distance = Vector3.Distance(player.transform.position, object_pos);
        //    transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(new Vector3(0, -angle - 90, 0)), 3600/distance);
        //}

        if (!mouseLock)
        {
            if (Input.GetMouseButtonDown(0))
            {
                startPos = Input.mousePosition;
            }
            else if (Input.GetMouseButton(0))
            {
                endPos = Input.mousePosition;
                dir = endPos - startPos;
                startPos = Input.mousePosition;

                currentAngle = currentCircle.eulerAngles.y;
                currentCircle.Rotate(0, dir.x * 0.25f, 0);
                lastAngle = currentCircle.eulerAngles.y;
                currentCircle.Rotate(0, dir.x * 0.001f, 0);

                myAngle += Mathf.Abs(lastAngle - currentAngle);

                if (myAngle >= 45f)
                {
                    myAngle = 0;
                }
            }
            else if (Input.GetMouseButtonUp(0))
            {
                startPos = endPos = Vector3.zero;
            }
        }
    }

    private void FixedUpdate()
    {
        if (lane1Stack != null && lane2Stack != null && isCheck)
        {
            Quaternion a = lane1Stack.transform.rotation;
            Quaternion b = lane2Stack.transform.rotation;

            float angle = Quaternion.Angle(a, b);

            bool sameRotation = Mathf.Abs(angle) > 170 && Mathf.Abs(angle) < 190;
            if (sameRotation)
            {
                var idLane2 = int.Parse(lane2Stack.gameObject.tag);
                idLane2++;
                LaneGenerator.instance.ChangeStack2(idLane2);
                //LaneGenerator.isStopSpawnMore = true;
                var temp = lane2Stack;
                temp.transform.DOScaleZ(transform.localScale.z * 1.5f, 1.9f);
                temp.transform.DOScaleX(transform.localScale.x * 1.5f, 1.9f);
                temp.GetComponent<MeshRenderer>().material.DOFade(0, 0.3f);
                temp.transform.GetChild(0).gameObject.GetComponent<MeshRenderer>().material.DOFade(0, 0.3f);
                //Destroy(temp.gameObject, 0.45f);
                var idLane1 = int.Parse(lane1Stack.gameObject.tag);
                idLane1++;
                LaneGenerator.instance.ChangeStack1(idLane1);
                idLane1-=2;
                Debug.Log(idLane1);
                StartCoroutine(delayShowStack(temp, idLane1, a));
            }
            isCheck = false;
            StartCoroutine(delayCheck());
        }
    }

    public void Win()
    {
        winPanel.SetActive(true);
        var currentScene = SceneManager.GetActiveScene().buildIndex;
        if(currentScene < 2)
        {
            currentScene++;
        }
        else
        {
            currentScene = 0;
        }
        StartCoroutine(delayLoadScene(currentScene));
    }

    public void Lose()
    {
        losePanel.SetActive(true);
        var currentScene = SceneManager.GetActiveScene().buildIndex;
        StartCoroutine(delayLoadScene(currentScene));
    }

    IEnumerator delayLoadScene(int scene)
    {
        mouseLock = true;
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene(scene);
    }

    IEnumerator delayShowStack(GameObject temp, int id, Quaternion original)
    {
        yield return new WaitForSeconds(0.3f);
        if (id < listMesh.Count)
        {
            temp.GetComponent<MeshFilter>().sharedMesh = listMesh[id];
            temp.transform.rotation = original;
            temp.transform.DOKill();
            temp.GetComponent<MeshRenderer>().material.DOKill();
            temp.transform.localScale = new Vector3(-5, 5, 5);
            temp.GetComponent<MeshRenderer>().material.DOFade(1, 0.1f);
        }
    }

    //private void OnTriggerStay(Collider other)
    //{
    //    if (other.tag != "Untagged" && other.transform.localPosition.y >= 0 && isCheck)
    //    {
    //        Quaternion a = lane1Stack.transform.rotation;
    //        Quaternion b = other.transform.rotation;

    //        float angle = Quaternion.Angle(a, b);

    //        bool sameRotation = Mathf.Abs(angle) > 170 && Mathf.Abs(angle) < 190;
    //        if (sameRotation)
    //        {
    //            var id = int.Parse(other.gameObject.tag);
    //            LaneGenerator.instance.AddLostList(id);
    //            LaneGenerator.isStopSpawnMore = true;
    //            other.transform.DOScaleZ(transform.localScale.z * 1.5f, 0.45f);
    //            other.transform.DOScaleX(transform.localScale.x * 1.5f, 0.45f);
    //            other.GetComponent<MeshRenderer>().material.DOFade(0, 0.45f);
    //            Destroy(other.gameObject, 0.45f);
    //        }
    //        isCheck = false;
    //        StartCoroutine(delayCheck());
    //    }
    //}
    public void GetLane1Stack(GameObject stack)
    {
        lane1Stack = stack;
        currentCircle = lane1Stack.transform.parent;
    }

    public void GetLane2Stack(GameObject stack)
    {
        StartCoroutine(delayChangeTarget(stack));
    }

    IEnumerator delayChangeTarget(GameObject stack)
    {
        yield return new WaitForSeconds(0.5f);
        lane2Stack = stack;
        var line = lane2Stack.transform.GetChild(0).gameObject;
        line.SetActive(true);
        line.GetComponent<MeshRenderer>().material.DOFade(0, 0);
        line.GetComponent<MeshRenderer>().material.DOFade(1, 0.25f);
    }

    IEnumerator delayCheck()
    {
        yield return new WaitForSeconds(0.5f);
        isCheck = true;
    }
}
