﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrignometricRotation : MonoBehaviour
{
    public TrignometricRotation instance;
    public Vector3 AngleLimit;
    public Vector3 RotationFrequency;
    Vector3 FinalRotation;
    Vector3 StartRotation;
    bool isTurn = false;
    void Start()
    {
        instance = this;
        StartRotation = transform.localEulerAngles;
        AngleLimit.y = Random.Range(180, 360);
        RotationFrequency.y = Random.Range(0.1f, 0.5f);
    }
    void Update()
    {
        if (isTurn)
        {
            Debug.Log("Turn 3");
            FinalRotation.x = StartRotation.x + Mathf.Sin(Time.timeSinceLevelLoad * RotationFrequency.x) * AngleLimit.x;
            FinalRotation.y = StartRotation.y + Mathf.Sin(Time.timeSinceLevelLoad * RotationFrequency.y) * AngleLimit.y;
            FinalRotation.z = StartRotation.z + Mathf.Sin(Time.timeSinceLevelLoad * RotationFrequency.z) * AngleLimit.z;
            var des = new Vector3(FinalRotation.x, FinalRotation.y, FinalRotation.z);
            //if (transform.localEulerAngles != des)
            //{
                transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(new Vector3(FinalRotation.x, FinalRotation.y, FinalRotation.z)), 1);
            //}
            //transform.localEulerAngles = new Vector3(FinalRotation.x, FinalRotation.y, FinalRotation.z);
        }
    }

    public void Switch(bool check)
    {
        isTurn = check;
        Debug.Log("Turn 2");
    }
}
