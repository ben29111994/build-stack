﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class LaneGenerator : MonoBehaviour {
    public static LaneGenerator instance;
    public float maxVariance = 0f;
    public int count = 16;
    public GameObject InitialColumn;

    public List<GameObject> m_ColumnList;
    private int m_OldestColumnIndex;
    private int m_NewestColumnIndex;
    private float m_Hue;
    private float saturation;
    private int targetCounter;
    private float m_LaneSize;
    public List<GameObject> laneList = new List<GameObject>();
    public List<GameObject> stackList = new List<GameObject>();
    int currentChild = 0;
    int currentLevel = 0;
    int firstSpawnCount = 0;
    public static bool isStopSpawnMore = false;
    public List<GameObject> buildList = new List<GameObject>();
    int laneID;
    Quaternion stack1Rotation;

    // Use this for initialization
    private void Start()
    {
        instance = this;
        // Start hue value, increment for each new row
        m_Hue = 0.5f;
        for (int i = 0; i < laneList.Count; i++)
        {
            var j = Random.Range(0, laneList[i].transform.childCount);
            //for (int j = 0; j < laneList[i].transform.childCount; j++)
            //{
                var child = laneList[i].transform.GetChild(j);
                stackList.Add(child.gameObject);
            //}
        }
        //}
        StartCoroutine(delayGrow());
        //StartCoroutine(delayChangeFogColor());
        currentLevel = PlayerPrefs.GetInt("currentLevel");
        firstSpawnCount = 16;
    }

    public void Install (Transform mother) {
        // Record lane width, which allows accurate positioning
        m_LaneSize = InitialColumn.transform.localScale.y;
        // Creat list of columns
        m_ColumnList = new List<GameObject>();
        count = 2;
        if(count > 10)
        {
            count = 10;
        }
        for (int i = 0; i < count; i++)
        {
            // First one exists, just add it and set it's color
            if (i == 0)
            {
                m_ColumnList.Add(InitialColumn);
            }
            // Others are created & added. Color is set by PlaceColumn
            else
            {
                GameObject newColumn = Instantiate(InitialColumn);               
                //if (i == count - 1)
                //{
                //    newColumn.tag = "Last";
                //}
                newColumn.transform.parent = mother;
                //newColumn.GetComponent<MeshCollider>().isTrigger = false;
                //newColumn.GetComponent<Rigidbody>().isKinematic = false;
                newColumn.GetComponent<MeshRenderer>().material.DOFade(0, 0);
                newColumn.GetComponent<MeshRenderer>().material.DOFade(1, 0.25f);
                m_ColumnList.Add(newColumn);
                PlaceColumn(m_ColumnList.Count - 1, m_ColumnList[m_ColumnList.Count - 2].transform.position, maxVariance);
                //newColumn.transform.localRotation = InitialColumn.transform.localRotation;
                newColumn.transform.rotation = Quaternion.Euler(new Vector3(0, Random.Range(0, 360), 0));

                //Quaternion a = newColumn.transform.rotation;
                //Quaternion b = stack1Rotation;

                //float angle = Quaternion.Angle(a, b);

                //bool sameRotation = Mathf.Abs(angle) > 170 && Mathf.Abs(angle) < 190;
                //while (sameRotation)
                //{
                //    newColumn.transform.rotation = Quaternion.Euler(new Vector3(0, Random.Range(0, 360), 0));
                //    a = newColumn.transform.rotation;
                //    b = stack1Rotation;

                //    angle = Quaternion.Angle(a, b);

                //    sameRotation = Mathf.Abs(angle) > 170 && Mathf.Abs(angle) < 190;
                //}
                //Debug.Log(laneID);
                if (laneID == 0)
                {
                    GameController.instance.GetLane1Stack(newColumn);
                    stack1Rotation = newColumn.transform.rotation;
                }
                if (laneID == 1)
                {
                    GameController.instance.GetLane2Stack(newColumn);
                }
                buildList.Add(newColumn);
            }
        }


        // Reference indexes for the newest and oldest columns. We move the oldest, and put it next to the newest.
        m_OldestColumnIndex = 0;
        m_NewestColumnIndex = m_ColumnList.Count - 1;
    }

    private void PlaceColumn(int columnIndex, Vector3 prevColumnPos, float maximumVariance )
    {
        // Get the desired position, one column width downstream from the latest column
        float offsetZ = 0;
        offsetZ = Random.Range(prevColumnPos.y - maximumVariance, prevColumnPos.y + maximumVariance);
        if(offsetZ > 2 || offsetZ < -2)
        {
            offsetZ = 0;
        }
        Vector3 movePos = new Vector3(prevColumnPos.x, prevColumnPos.y + m_LaneSize - 2, prevColumnPos.z/*offsetZ*/);
        // Move to that new position
        m_ColumnList[columnIndex].transform.position = movePos;
        // Set new color (with increment hue)
        //SetColor(columnIndex);
        // Keep track of how many rows are generated since the last target.
        targetCounter++;
        // If the row contains a target, remove it.
        if (m_ColumnList[columnIndex].transform.Find(columnIndex.ToString()))
        {
            Destroy(m_ColumnList[columnIndex].transform.Find(columnIndex.ToString()).gameObject);
        }
    }

    public void ChangeStack1(int id)
    {
        Debug.Log("stack1: " + id);
        if (id < buildList.Count)
            GameController.instance.GetLane1Stack(buildList[id]);
        else
            GameController.instance.Win();
    }

    public void ChangeStack2(int id)
    {
        Debug.Log("stack2: " + id);
        if (id < buildList.Count)
            GameController.instance.GetLane2Stack(buildList[id]);
        else
            GameController.instance.Win();
    }

    IEnumerator delayGrow()
    {
        Transform temp;
        for (int i = 0; i < stackList.Count; i++)
        {
            if (firstSpawnCount <= 0)
            {
                yield return new WaitForSeconds(1f);
            }
            else
            {
                firstSpawnCount--;
            }
            InitialColumn = stackList[i].gameObject;
            laneID = int.Parse(stackList[i].tag);
            laneID--;
            temp = laneList[laneID].transform;
            Install(temp);
        }
    }

    //IEnumerator delayFix()
    //{
    //    Transform temp;
    //    Debug.Log("Spawn Inside!");
    //    yield return new WaitForSeconds(1);
    //    InitialColumn = stackList[buildList[0]].gameObject;
    //    laneID = int.Parse(stackList[buildList[0]].tag);
    //    laneID--;
    //    temp = laneList[laneID].transform;
    //    for (int j = 0; j < laneList.Count; j++)
    //    {
    //        if (j == laneID)
    //        {
    //            //laneList[j].GetComponent<TrignometricRotation>().instance.Switch(true);
    //            Debug.Log("Turn 1");
    //        }
    //        else
    //        {
    //            //laneList[j].GetComponent<TrignometricRotation>().instance.Switch(false);
    //        }
    //    }
    //    Install(temp);
    //    buildList.Remove(buildList[0]);
    //    buildList.TrimExcess();
    //    isStopSpawnMore = false;
    //}

    private void SetColor(int columnIndex)
    {
        // increment the hue, looping back to 0 if necessary
        m_Hue += (1f / 20f);
        if (m_Hue > 1f)
            m_Hue = 0;
        // Default saturation
        saturation = .7f;
        // Alternative saturation for even numbers
        if (columnIndex % 2 == 0)
        {
            saturation = .5f;
        }
        // Set color for each child's renderer
        foreach (MeshRenderer renderer in m_ColumnList[columnIndex].GetComponentsInChildren<MeshRenderer>())
        {
            renderer.material.color = Color.HSVToRGB(m_Hue, saturation, .7f);
        }
    }

    IEnumerator delayChangeFogColor()
    {
        m_Hue += (1f / 20f);
        if (m_Hue > 1f)
            m_Hue = 0;
        yield return new WaitForSeconds(3);
        DOTween.To(() => RenderSettings.fogColor, x => RenderSettings.fogColor = x, Color.HSVToRGB(m_Hue, saturation, .7f), 3f);
        StartCoroutine(delayChangeFogColor());
    }
}
