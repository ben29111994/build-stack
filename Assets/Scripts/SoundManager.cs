﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {

	public static SoundManager instance;

	public AudioSource audioSource;
    public AudioClip hit, win, lose, cash;
    public float pitchTimeOut = 0;

    void Awake(){
        if (instance)
        {
            DestroyImmediate(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        try
        {
            audioSource = GetComponent<AudioSource>();
        }
        catch
        {

        }      
    }

    public void PlaySound(AudioClip clip){
        if (clip == hit)
        {
            if (pitchTimeOut == 0)
            {
                audioSource.pitch = 1;
            }
            audioSource.pitch += 0.0001f;
            pitchTimeOut = 1;
        }
        else
            audioSource.pitch = 1;
        audioSource.PlayOneShot(clip);
	}

    private void LateUpdate()
    {
        if (pitchTimeOut > 0)
        {
            pitchTimeOut -= 0.05f;
        }
    }
}
